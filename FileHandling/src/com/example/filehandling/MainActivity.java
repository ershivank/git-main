package com.example.filehandling;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
public class MainActivity extends Activity {
	//protected static final String Log = null;
	String FILENAME="mydata.txt";
	 String mydata="";
	 Button r,w;
	 EditText d;
			


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        r=(Button)findViewById(R.id.button1);
        w=(Button)findViewById(R.id.button2);
        d=(EditText)findViewById(R.id.editText1);
      w.setOnClickListener( new View.OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			try{
				FileOutputStream fos=openFileOutput(FILENAME, Context.MODE_PRIVATE);
				fos.write(mydata.getBytes());
			fos.close();
			}catch(Exception ex){
			Log.e(" exception",ex.toString());
			
			}
		}	
			});
      
      r.setOnClickListener( new View.OnClickListener(){
    	  @Override
  		public void onClick(View arg0) {
    	  try{
				FileInputStream fis=openFileInput(FILENAME);
				byte[] reader = new byte[fis.available()];
				if(fis.read(reader)!=-1)
				{
					mydata = new String(reader);
					d.setText(mydata);
				}
			    fis.close();
			}catch(Exception ex)
			{
			 Log.i(" exception",ex.toString());
			 
			}
    	  }
		});
      }
    }	  
			
      


   
 