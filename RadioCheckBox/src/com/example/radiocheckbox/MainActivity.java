package com.example.radiocheckbox;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class MainActivity extends Activity {
	Button ok;
	EditText t1;
	RadioButton rm,rb;
	CheckBox cb1,cb2,cb3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
      ok=(Button)findViewById(R.id.button1);
      t1 =(EditText)findViewById(R.id.editText1);
     rm =(RadioButton)findViewById(R.id.radio0);
     rb =(RadioButton)findViewById(R.id.radio1);
     cb1 =(CheckBox)findViewById(R.id.checkBox1);
     cb2 =(CheckBox)findViewById(R.id.checkBox2);
     cb3 =(CheckBox)findViewById(R.id.checkBox3);
     ok.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			
			String n1="Name\n"+t1.getText().toString();
			n1 = n1 + "\nGender";
			if(rm.isChecked())
				n1 = n1 + "\n" +rm.getText();
			
			if(rb.isChecked())
				n1 = n1 + "\n" +rb.getText();
			n1 = n1 + "\nchoice";
			if(cb1.isChecked())
				n1=n1+"\n" +cb1.getText();
			
			if(cb2.isChecked())
				n1=n1+"\n" +cb2.getText();
			if(cb3.isChecked())
				n1=n1+"\n" +cb3.getText();
			showMessage(n1);
		}
	});
    }
public void showMessage(String msg) {
	AlertDialog.Builder b = new AlertDialog.Builder(this);
	b.setTitle("Information");
	b.setMessage(msg);
	b.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface arg0, int arg1) {
		Toast t =	Toast.makeText(getApplicationContext(),"You Clicked the Cancel Button", Toast.LENGTH_LONG);
			t.show();
			
		}
	});
	b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface arg0, int arg1) {
	     Toast.makeText(getApplicationContext(),"You Clicked the Ok Button", Toast.LENGTH_LONG).show();
		
			
		}
	});
	b.show();
}
}
   