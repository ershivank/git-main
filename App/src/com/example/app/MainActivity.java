package com.example.app;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
    EditText name;
    Button ok,clear;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        name=(EditText)findViewById(R.id.editText1);
        ok=(Button)findViewById(R.id.button1);
        clear=(Button)findViewById(R.id.button2);
        
        ok.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				String str=name.getText().toString();
				   Toast.makeText(getApplication(), str, 60).show();
			   
			}
		});
        
        clear.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				name.setText("");
				
			}
		});
    }


 /*  public void ok(View v){
	   String str=name.getText().toString();
	   Toast.makeText(getApplication(), str, 60).show();
   }
public void clear(View v){
	   name.setText("");
   }  */
}
