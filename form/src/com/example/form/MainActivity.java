package com.example.form;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;

public class MainActivity extends Activity {
	Button b1,b2;
	RadioButton r1,r2;
	CheckBox cb1,cb2,cb3;
	EditText ed1;
	


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        b1=(Button)findViewById(R.id.button1);
       r1=(RadioButton)findViewById(R.id.radioButton1);
       r2=(RadioButton)findViewById(R.id.radioButton2);
       cb1=(CheckBox)findViewById(R.id.checkBox1);
       cb2=(CheckBox)findViewById(R.id.checkBox2);
       cb3=(CheckBox)findViewById(R.id.checkBox3);
       ed1=(EditText)findViewById(R.id.editText1);
     //  b2.setOnClickListener(new View.OnClickListener() {
		
		/*@Override
		public void onClick(View arg0) {
			
			ed1.setText("");
		}});
}*/
	
       b1.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			String n1="name"+ed1.getText().toString();
			n1=n1+"\ngender";
			if(r1.isChecked())
				n1=n1+"\n"+r1.getText();
			if(r2.isChecked())
				n1=n1+"\n"+r2.getText();
			if(cb1.isChecked())
				n1=n1+"\n"+cb1.getText();
			if(cb2.isChecked())
				n1=n1+"\n"+cb2.getText();
			if(cb3.isChecked())
				n1=n1+"\n"+cb3.getText();
			showMessage(n1);
		}
		});
	}
       
       public void showMessage(String msg) {
    		AlertDialog.Builder b = new AlertDialog.Builder(this);
    		b.setTitle("Information");
    		b.setMessage(msg);
    		b.show();
    	}
    	}

       
