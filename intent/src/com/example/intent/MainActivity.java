package com.example.intent;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {

	EditText t1,t2,t3,t4;
	Button b1,b2;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        b1=(Button)findViewById(R.id.go);
        b2=(Button)findViewById(R.id.backhome);
        t1=(EditText)findViewById(R.id.name);
        t2=(EditText)findViewById(R.id.add);
        t3=(EditText)findViewById(R.id.num);
        t4=(EditText)findViewById(R.id.msg);
   
    }
    public void go(View view)
     
    {
    //Intent i=new Intent("com.example.intent.second");
    	Intent i=new Intent(MainActivity.this,Second.class);
    startActivity(i);
    
    }
}

    