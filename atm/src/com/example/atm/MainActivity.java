package com.example.atm;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {
	Button b1;
	
	

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       
    }

public void deposit(View view)
{
	Intent i=new Intent("com.example.atm.DepositActivity");
	startActivity(i);
	
}
public void withdraw(View view)
{
	Intent i=new Intent("com.example.atm.Withdraw");
	startActivity(i);
}
	
	public void balance (View view)
	{
		Intent i=new Intent("com.example.atm.Balance");
		startActivity(i);
}

public void changepin(View view)
{
	Intent i=new Intent("com.example.atm.ChangePin");
	startActivity(i);
}}

