package com.example.myquiz;

import android.R.string;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends Activity {
	TextView questv;
	RadioButton first,Second,Third;
	Button Pre,next,end;
	String ques[]= {"What is PM name", "Which is national bird?", "what is national fruit?","what is national flower?"};
	string op[][]=
		{
			{"Sonia","Modi","Rahul"},{"crow","peacock","parrot"},{"mango","apple","pear"},{"rose","lotus","lily"}
			
		};
	String ans={"Modi","peacock","mango","lotus"};
	int i=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        questv=(TextView)findViewById(R.id.tv1);
        first=(RadioButton)findViewById(R.id.radio0);
       Second=(RadioButton)findViewById(R.id.radio1);
        Third=(RadioButton)findViewById(R.id.radio2);
        Pre=(Button)findViewById(R.id.button1);
        next=(Button)findViewById(R.id.button2);
       end=(Button)findViewById(R.id.button3);
       questv.setText(ques[i]);
       first.setText(op[i][0]);
       Second.setText(op[i][1]);
      Third.setText(op[i][2]);
       
       
       
       Pre.setOnClickListener(new View.OnClickListener() {
	   	
	
		public void onClick(View arg0) {
			    if(i==0)
				Pre.setEnabled(false);
			    else 
			    
			    {
			    	
			    	Pre.setEnabled(true);
			questv.setText(ques[--i]);
			}
			    first.setText(op[i][0]);
			       Second.setText(op[i][1]);
			      Third.setText(op[i][2]);
			    
			
		}
	});
       
       next.setOnClickListener(new View.OnClickListener() {
		
	      
		public void onClick(View arg0) {
			if(i== ques.length -1)
				next.setEnabled(false);
			else
			{
				next.setEnabled(true);
				
			
			questv.setText(ques[++i]);}
			 first.setText(op[i][0]);
		       Second.setText(op[i][1]);
		      Third.setText(op[i][2]);
		}
	});
       
        
        
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}
