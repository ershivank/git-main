package com.example.power;

import com.example.power.R;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
 EditText base,exp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        base=(EditText)findViewById(R.id.editText1);
        exp=(EditText)findViewById(R.id.editText2);
    }

public void result(View view)
{ double n1,n2;
n1=Double.parseDouble(base.getText().toString());
n2=Double.parseDouble(exp.getText().toString());

 double k;
	k=Math.pow(n1,n2);
	Toast.makeText(getApplication(),""+k, 60).show();
	
}
 
public void clear(View view)
{
base.setText("");
exp.setText("");
 }

}