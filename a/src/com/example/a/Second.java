package com.example.a;

 import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class Second extends Activity {

	   Button btnBack;
	   EditText editMsg;
	   String name;
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.second);
	        btnBack=(Button)findViewById(R.id.btnBack);
	        editMsg=(EditText)findViewById(R.id.editMsg);
	        Intent i=getIntent();
	        Bundle b=i.getExtras();
	        name=b.getString("name");
	        String city=b.getString("city");
	        String pin=b.getString("pin");
	        String s=name+"\n"+city+"\n"+pin;
	        showMessage(s);
	        btnBack.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					
				  onBackPressed();	
				}
			});
	    }
public void showMessage(String msg){
	AlertDialog.Builder builder=new AlertDialog.Builder(this);
	builder.setMessage(msg);
	builder.setTitle("Information");
	
	builder.show();
}

@Override
public void finish() {
    
    Intent data = new Intent();
    data.putExtra("msg", "shivank "+name+", hello!"+editMsg.getText().toString());
   
    this.setResult(Activity.RESULT_OK, data);
    super.finish();
}}
 


