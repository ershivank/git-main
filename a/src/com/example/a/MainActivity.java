package com.example.a;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity implements OnClickListener{

   Button buttonGo;
   EditText editName,editCity,editPin;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonGo=(Button)findViewById(R.id.buttonGo);
        editName=(EditText)findViewById(R.id.editName);
        editCity=(EditText)findViewById(R.id.editCity);
        editPin=(EditText)findViewById(R.id.editPin);
        buttonGo.setOnClickListener(this);
    }
	   protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	       if (resultCode == Activity.RESULT_OK && requestCode == 1 ) {
	           String feedback = data.getStringExtra("msg");
	           this.editName.setText(feedback);
	       } else {
	           this.editName.setText("!?");
	       }
	   }
	@Override
	public void onClick(View arg0) {
		String name=editName.getText().toString();
		String city=editCity.getText().toString();
		String pin=editPin.getText().toString();
		Intent i=new Intent(MainActivity.this,Second.class);//declaration method
		Bundle b =new Bundle();
		b.putString("name", name);
		b.putString("city", city);
		b.putString("pin", pin);
		i.putExtras(b);
	
		startActivityForResult(i,1);
	}}

