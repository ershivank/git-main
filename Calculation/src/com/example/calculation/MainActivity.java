package com.example.calculation;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {
   EditText t1,t2;
   TextView out;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        t1=(EditText)findViewById(R.id.editText1);
        t2=(EditText)findViewById(R.id.editText2);
        out=(TextView)findViewById(R.id.textView3);
        
    }

public void add(View view){
 
	String str=t1.getText().toString();
	int x=Integer.parseInt(str);
	str=t2.getText().toString();
	int y=Integer.parseInt(str);
	int z=x+y;
	out.setText(""+z);
}   
   
public void sub(View view){
	 
	String str=t1.getText().toString();
	int x=Integer.parseInt(str);
	str=t2.getText().toString();
	int y=Integer.parseInt(str);
	int z=x-y;
	out.setText(""+z);
}   
public void multiple(View view){
	 
	String str=t1.getText().toString();
	int x=Integer.parseInt(str);
	str=t2.getText().toString();
	int y=Integer.parseInt(str);
	int z=x*y;
	out.setText(""+z);
}
public void divide(View view){
	 
	String str=t1.getText().toString();
	int x=Integer.parseInt(str);
	str=t2.getText().toString();
	int y=Integer.parseInt(str);
	int z=x/y;
	out.setText(""+z);
}
public void clear(View view){
	 
	t1.setText("");
	t2.setText("");
	
}	
}
