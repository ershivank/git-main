package com.example.checkeven;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	Button b1,b2;
	EditText t1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        b1=(Button)findViewById(R.id.button1);
        b2=(Button)findViewById(R.id.button2);
        t1=(EditText)findViewById(R.id.editText1);
        b1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
			    result();
			}
		});
        b2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				clear();
			}
		});
    }
     public void result()
    {
    	int n;
    	
    	n=Integer.parseInt(t1.getText().toString());
    	if(n%2==0)
    	{
    		String res = "Even" + ""+n;
    		Toast.makeText(getApplication(), res, Toast.LENGTH_LONG).show();
    	}
    	else
    	{
    		String res = "Odd" +n;
    		Toast.makeText(getApplication(), res, Toast.LENGTH_LONG).show();
    	}
    }
    public void clear()
    {
    	t1.setText("");
    }}