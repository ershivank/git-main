package com.example.secondpro;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Color;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {
Button b1,b2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        b1=(Button)findViewById(R.id.button1);
        b2=(Button)findViewById(R.id.button2);
        b1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				b1.setBackgroundColor(Color.rgb(50, 150, 25));
			}
	
        });
        b2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				b2.setBackgroundColor(Color.rgb(250, 150,50));	
			}
		});
        public void change(View v) {
        	b2.setBackgroundColor(Color.rgb(255, 255,255));	
        	b1.setBackgroundColor(Color.rgb(255, 255,255));	
       
        }
    }


  
}
