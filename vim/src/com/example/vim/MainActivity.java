package com.example.vim;



import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

     EditText t1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        t1=(EditText)findViewById(R.id.editText1);
    }

    public void fact(View view)
    {
    	String str=t1.getText().toString();
    	int x=Integer.parseInt(str);
    	int f=1;
    	for(int i=1;i<=x;i++)
    	      f=f*i;
    	//String out=""+f;
    	Toast.makeText(getApplication()," ANSWER IS  "+f, Toast.LENGTH_LONG).show();
    }

       public void delete(View view)
       {
    	   t1.setText("");
       }  
}
